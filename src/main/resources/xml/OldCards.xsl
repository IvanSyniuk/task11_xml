<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    table.tfmt {
                    border: 3px ;
                    }

                    td.colfmt {
                    font-size:20px;
                    border: 3px ;
                    background-color: grey;
                    color: purple;
                    text-align:right;
                    }

                    th {
                    background-color: black;
                    color: white;
                    }

                </style>
            </head>

            <body>
                <table class="oldCards">
                    <tr>
                        <th style="width:250px">id</th>
                        <th style="width:250px">country</th>
                        <th style="width:250px">type</th>
                        <th style="width:250px">author name</th>
                        <th style="width:250px">theme</th>
                        <th style="width:250px">valueble</th>
                        <th style="width:250px">year</th>


                    </tr>

                    <xsl:for-each select="oldCards/oldCard">

                        <tr>
                            <td class="colfmt">
                                <xsl:value-of select="@oldCardId"/>
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="country" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="type/typeName" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="author/authorName" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="type/theme" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="type/valueble" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="year" />
                            </td>

                        </tr>

                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
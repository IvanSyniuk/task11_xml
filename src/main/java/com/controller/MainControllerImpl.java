package com.controller;

import com.comparator.OldCardComparator;
import com.model.OldCard;
import com.parser.dom.DomReader;
import com.parser.sax.SaxParser;
import com.parser.stax.StaxParser;
import com.parser.xmlvalidator.XmlValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.Scanner;

public class MainControllerImpl implements MainController {
    private static Logger logger = LogManager.getLogger(MainControllerImpl.class);
    private Scanner sc = new Scanner(System.in);

    @Override
    public void chooseParser() {
        DomReader domReader = new DomReader();
        OldCardComparator comparator = new OldCardComparator();
        File xmlFile = new File("src\\main\\resources\\xml\\oldCards.xml");
        File xsdFile = new File("src\\main\\resources\\xml\\oldCards.xsd");

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder;
        Document document = null;
        try {
            documentBuilder = factory.newDocumentBuilder();
            document = documentBuilder.parse(xmlFile);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        if (XmlValidator.validate(document, xsdFile)) {
            logger.info("1:DOM\n2:SAX\n3:STAX\n");
            switch (sc.nextInt()){
                case 1:
                    logger.info("/////////////////DOM//////////////");
                    List<OldCard> oldCards = domReader.readDoc(document);
                    oldCards.sort(comparator);
                    logger.info(oldCards);
                    break;
                case 2:
                    logger.info("//////////SAX/////////////");
                    List<OldCard> oldCardsSAX = SaxParser.parse(xmlFile, xsdFile);
                    oldCardsSAX.sort(comparator);
                    logger.info(oldCardsSAX);
                    break;
                case 3:
                    logger.info("////////////STAX///////////");
                    StaxParser staxParser = new StaxParser();
                    List<OldCard> oldCardSTAX = staxParser.parse(xmlFile);
                    oldCardSTAX.sort(comparator);
                    logger.info(oldCardSTAX);
                    break;
            }

        } else {
            logger.error("XML document failed validation.");
        }
    }

    @Override
    public void convertToHtml() {
        Source xml = new StreamSource(new File("src\\main\\resources\\xml\\oldCards.xml"));
        Source xslt = new StreamSource("src\\main\\resources\\xml\\oldCards.xsl");
        StringWriter sw = new StringWriter();
        try {
            FileWriter fw = new FileWriter("src\\main\\resources\\xml\\oldCards.html");
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer trasform = tFactory.newTransformer(xslt);
            trasform.transform(xml, new StreamResult(sw));
            fw.write(sw.toString());
            fw.close();
            logger.info("oldCards.html generated successfully.");
        } catch (IOException | TransformerFactoryConfigurationError | TransformerException   e) {
            e.printStackTrace();
        }
    }
}
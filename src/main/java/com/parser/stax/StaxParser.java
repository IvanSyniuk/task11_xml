package com.parser.stax;

import com.model.Author;
import com.model.OldCard;
import com.model.Type;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StaxParser {

    static final String OLDCARD_TAG = "oldCard";
    static final String COUNTRY_TAG = "country";
    static final String YEAR_TAG = "year";
    static final String AUTHORNAME_TAG = "authorName";
    static final String AUTHOR_TAG = "author";
    static final String THEME_TAG = "theme";
    static final String TYPE_TAG = "type";
    static final String TYPENAME_TAG = "typeName";
    static final String VALUEBLE_TAG = "valueble";
    static final String ISSPENDED_TAG = "isSpended";
    static final String OLDCARD_ID_ATTRIBUTE = "oldCardId";

    private List<OldCard> oldCards = new ArrayList<>();
    private OldCard oldCard;
    private Type type;
    private List<Author> authors;


    private XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

    public List<OldCard> parse(File xml) {
        try {
            XMLEventReader xmlEventReader = xmlInputFactory
                    .createXMLEventReader(new FileInputStream(xml));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case OLDCARD_TAG: {
                            oldCard = new OldCard();
                            Attribute oldCardId = startElement.getAttributeByName(new QName(OLDCARD_ID_ATTRIBUTE));
                            if (oldCardId != null) {
                                oldCard.setOldCardId(Integer.parseInt(oldCardId.getValue()));
                            }
                            break;
                        }
                        case COUNTRY_TAG: {
                            xmlEvent = xmlEventReader.nextEvent();
                            oldCard.setCountry(xmlEvent.asCharacters().getData());
                            break;
                        }
                        case YEAR_TAG: {
                            xmlEvent = xmlEventReader.nextEvent();
                            oldCard.setYear(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        }
                        case TYPE_TAG: {
                            type = new Type();
                            break;
                        }
                        case TYPENAME_TAG: {
                            xmlEvent = xmlEventReader.nextEvent();
                            type.setCardType(xmlEvent.asCharacters().getData());
                            break;
                        }
                        case ISSPENDED_TAG: {
                            xmlEvent = xmlEventReader.nextEvent();
                            type.setSpended(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        }
                        case VALUEBLE_TAG: {
                            xmlEvent = xmlEventReader.nextEvent();
                            type.setValueble(xmlEvent.asCharacters().getData());
                            break;
                        }
                        case THEME_TAG: {
                            xmlEvent = xmlEventReader.nextEvent();
                            type.setTheme(xmlEvent.asCharacters().getData());
                            break;
                        }
                        case AUTHOR_TAG: {
                            authors= new ArrayList<>();
                            break;
                        }
                        case AUTHORNAME_TAG: {
                            xmlEvent = xmlEventReader.nextEvent();
                            Author author = new Author(xmlEvent.asCharacters().getData());
                            authors.add(author);
                            break;
                        }
                    }
                }

                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    String name = endElement.getName().getLocalPart();
                    switch (name) {
                        case OLDCARD_TAG: {
                            oldCards.add(oldCard);
                            oldCard = null;
                            break;
                        }
                        case TYPE_TAG: {
                            oldCard.setType(type);
                            type = new Type();
                            break;
                        }
                        case AUTHOR_TAG: {
                            oldCard.setAuthors(authors);
                            authors = new ArrayList<>();
                            break;
                        }
                    }
                }
            }
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        }
        return oldCards;
    }
}
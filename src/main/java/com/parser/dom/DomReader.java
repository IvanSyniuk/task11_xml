package com.parser.dom;

import com.model.Author;
import com.model.OldCard;
import com.model.Type;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DomReader {
    public List<OldCard> readDoc(Document doc){
        doc.getDocumentElement().normalize();
        List<OldCard> oldCards = new ArrayList<>();

        NodeList nodeList = doc.getElementsByTagName("oldCard");

        for (int i = 0; i < nodeList.getLength(); i++) {
            OldCard oldCard = new OldCard();
            Type type;
            List<Author> authors;

            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element) node;

                oldCard.setOldCardId(Integer.parseInt(element.getAttribute("oldCardId")));
                oldCard.setCountry(element.getElementsByTagName("country").item(0).getTextContent());
                oldCard.setYear(Integer.parseInt(element.getElementsByTagName("year").item(0).getTextContent()));

                authors = getAuthors(element.getElementsByTagName("author"));
                type = getTypes(element.getElementsByTagName("type"));

                oldCard.setType(type);
                oldCard.setAuthors(authors);
                oldCards.add(oldCard);
            }
        }
        return oldCards;
    }

    private List<Author> getAuthors(NodeList nodes) {
        List<Author> authors = new ArrayList<>();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            NodeList nodeList = element.getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element el = (Element) node;
                    authors.add(new Author(el.getTextContent()));
                }
            }
        }
        return authors;
    }

    private Type getTypes(NodeList nodes) {
        Type type = new Type();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            type.setCardType(element.getElementsByTagName("typeName").item(0).getTextContent());
            type.setSpended(Boolean.parseBoolean(element.getElementsByTagName("isSpended").item(0).getTextContent()));
            type.setTheme(element.getElementsByTagName("theme").item(0).getTextContent());
            type.setValueble(element.getElementsByTagName("valueble").item(0).getTextContent());
        }
        return type;
    }

}
package com.parser.sax;

import com.model.Author;
import com.model.OldCard;
import com.model.Type;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SaxHandler extends DefaultHandler {

    static final String OLDCARD_TAG = "oldCard";
    static final String COUNTRY_TAG = "country";
    static final String YEAR_TAG = "year";
    static final String AUTHORNAME_TAG = "authorName";
    static final String AUTHOR_TAG = "author";
    static final String THEME_TAG = "theme";
    static final String TYPE_TAG = "type";
    static final String TYPENAME_TAG = "typeName";
    static final String VALUEBLE_TAG = "valueble";
    static final String ISSPENDED_TAG = "isSpended";
    static final String OLDCARD_ID_ATTRIBUTE = "oldCardId";

    private List<OldCard> oldCards = new ArrayList<>();
    private OldCard oldCard;
    private Type type;
    private List<Author> authors;
    private String currentElement;

    public List<OldCard> getOldCardsList() {
        return this.oldCards;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {
        currentElement = qName;

        switch (currentElement) {
            case OLDCARD_TAG: {
                String oldCardId = attributes.getValue(OLDCARD_ID_ATTRIBUTE);
                oldCard= new OldCard();
                oldCard.setOldCardId(Integer.parseInt(oldCardId));
                break;
            }
            case TYPE_TAG: {
                type = new Type();
                break;
            }
            case AUTHOR_TAG: {
                authors = new ArrayList<>();
                break;
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case OLDCARD_TAG: {
                oldCards.add(oldCard);
                break;
            }
            case TYPE_TAG: {
                oldCard.setType(type);
                type = new Type();
                break;
            }
            case AUTHOR_TAG: {
                oldCard.setAuthors(authors);
                authors = new ArrayList<>();
                break;
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (currentElement.equals(COUNTRY_TAG)) {
            oldCard.setCountry(new String(ch, start, length));
        }
        if (currentElement.equals(TYPENAME_TAG)) {
            type.setCardType(new String(ch, start, length));
        }
        if (currentElement.equals(YEAR_TAG)) {
            oldCard.setYear(Integer.parseInt(new String(ch, start, length)));
        }
        if (currentElement.equals(VALUEBLE_TAG)) {
            type.setValueble(new String(ch, start, length));
        }
        if (currentElement.equals(THEME_TAG)) {
            type.setTheme(new String(ch, start, length));
        }
        if (currentElement.equals(ISSPENDED_TAG)) {
            type.setSpended(Boolean.parseBoolean(new String(ch, start, length)));
        }
        if (currentElement.equals(AUTHORNAME_TAG)) {
            Author author = new Author(new String(ch, start, length));
            authors.add(author);
        }
    }
}
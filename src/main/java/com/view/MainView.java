package com.view;

import com.controller.MainControllerImpl;
import org.apache.logging.log4j.*;

import java.util.Scanner;

public class MainView {
    private static Logger logger = LogManager.getLogger(MainView.class);
    private MainControllerImpl mainController;
    private Scanner scanner;

    public MainView() {
        scanner = new Scanner(System.in);
        mainController = new MainControllerImpl();
    }

    public void showMenu() {
    logger.info("1:parsers\n2:convert to html\n3:Exit");
        boolean flag = true;
        do {
            switch (scanner.nextInt()) {
                case 1:
                    mainController.chooseParser();
                    break;
                case 2:
                    mainController.convertToHtml();
                    break;
                case 3:
                    flag = false;
            }
        } while (flag);
    }
}

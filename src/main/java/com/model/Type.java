package com.model;

public class Type {
    private String cardType;
    private String theme;
    private String valueble;
    private boolean isSpended;

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public void setValueble(String valueble) {
        this.valueble = valueble;
    }

    public void setSpended(boolean spended) {
        isSpended = spended;
    }

    @Override
    public String toString() {
        return "Type{" +
                "cardType='" + cardType + '\'' +
                ", theme='" + theme + '\'' +
                ", valueble='" + valueble + '\'' +
                ", isSpended=" + isSpended +
                '}';
    }
}

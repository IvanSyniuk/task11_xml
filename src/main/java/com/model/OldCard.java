package com.model;

import java.util.List;

public class OldCard {
    private int oldCardId;
    private int year;
    private String country;
    private Type type;
    private List<Author> authors;

    public void setOldCardId(int oldCardId) {
        this.oldCardId = oldCardId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    @Override
    public String toString() {
        return "OldCard{" +
                "oldCardId=" + oldCardId +
                ", year=" + year +
                ", country='" + country + '\'' +
                ", type=" + type +
                ", authors=" + authors +
                '}' + "\n";
    }
}